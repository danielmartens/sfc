# Choose Classifier Here and Give it a Name
from sklearn.ensemble import RandomForestClassifier
import pandas as pd
name = "Random Forest "

def classifier_rf(x_train, y_train, x_test):
    print("Start",name,"Classifier:")
    model =RandomForestClassifier(n_jobs=-1)

    print("Train",name,"Classifier")
    trained = model.fit(x_train, y_train)

    print("Test",name,"Classifier")
    y_test = pd.DataFrame(trained.predict_proba(x_test), index=x_test.index, columns=model.classes_)

    return y_test
