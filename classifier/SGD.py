# Choose Classifier Here and Give it a Name
from sklearn.linear_model import SGDClassifier
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix

name = "Stochastic Gradient Descent "

def classifier_sgd(x_train, y_train, x_test, y_test, x_k_test):
    print("Start",name,"Classifier:")
    model =SGDClassifier(loss='log', n_jobs=-1)

    print("Train",name,"Classifier")
    trained = model.fit(x_train, y_train)

    print("Test", name, "Classifier")
    pred = trained.predict(x_test)


    print("Evaluate", name, "Classifier")
    print("Confusion Matrix")
    print(confusion_matrix(y_test, pred))
    print("Accuracy")
    print(accuracy_score(y_test, pred))


    y_k_test = pd.DataFrame(trained.predict_proba(x_k_test), index=x_k_test.index, columns=model.classes_)
    return y_k_test
