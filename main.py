from features.Baseline import categorials
from DataGeneration import load_training_data
from DataGeneration import data_split
from DataGeneration import load_k_testing_data
from CSVWriter import write_results
from classifier.RandomForest import classifier_rf
from classifier.SGD import classifier_sgd
from classifier.SGD_kaggle import classifier_sgd_kaggle

# loading training data
x, y = load_training_data()

# test train split
#x_train, x_test, y_train, y_test = data_split(x, y)

# loading kaggle test data
x_k_test = load_k_testing_data()

# feature generation
x = categorials(x)
#x_test = categorials(x_test)
x_k_test = categorials(x_k_test)

x_train, x_test, y_train, y_test = data_split(x, y)

# model fit and prediction
#y_test = classifier_rf(x_train, y_train, x_test, y_test, x_k_test)
y_k_test = classifier_sgd(x_train, y_train, x_test, y_test, x_k_test)
#y_k_test = classifier_sgd_kaggle(x, y, x_k_test)


# save the result
write_results(y_k_test)
