import pandas as pd
from sklearn.cross_validation import train_test_split

def load_training_data():
    print('Loading training data...')

    # loading training data
    data = pd.read_csv('data/train.csv')

    X_train = data
    y_train = data["Category"]
    X_train = X_train.drop("Category", axis=1)

    # drop columns not in test data for now
    X_train = X_train.drop("Descript", axis=1)
    X_train = X_train.drop("Resolution", axis=1)

    return X_train, y_train

def load_k_testing_data():
    print('Loading testing data...')

    # loading test data
    data = pd.read_csv('data/test.csv', index_col="Id")
    X_k_test = data

    return X_k_test

def data_split(X, y):
    print('Splitting data...')

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

    return X_train, X_test, y_train, y_test