import pandas as pd
import numpy as np
import holidays
from address import AddressParser
from collections import Counter

def feature_holidays(data):
    print('Generating Holiday Feature')
    us_holidays = holidays.US(state='CA', years={2003, 2004, 2005, 2006, 2007,2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015})
    data['Dates'] = pd.to_datetime(data['Dates'], format='%Y-%m-%d')
    d = np.asarray(data['Dates'].dt.date.isin(us_holidays))
    return d, ["holiday"]

def feature_street_corner(data):
    print('Generating Street Corner Features')
    d = np.asarray(data['Address'].apply(lambda x: 1 if '/' in x else 0))
    print(Counter(d))
    return d, ["Street Type"]





