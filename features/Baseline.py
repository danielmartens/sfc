import pandas as pd
import numpy as np

def categorials(X_in):
    print("Generating features...")

    X = X_in

    # build categorical featuresa
    hours = pd.get_dummies(X.Dates.map(lambda x: pd.to_datetime(x).hour), prefix="hour")
    months = pd.get_dummies(X.Dates.map(lambda x: pd.to_datetime(x).month), prefix="month")
    years = pd.get_dummies(X.Dates.map(lambda x: pd.to_datetime(x).year), prefix="year")
    district = pd.get_dummies(X["PdDistrict"])
    day_of_week = pd.get_dummies(X["DayOfWeek"])

    # string them all together
    X = pd.concat([X, hours, months, years, district, day_of_week], axis=1)

    X = X.drop("PdDistrict", axis=1)
    X = X.drop("Address", axis=1)
    X = X.drop("Dates", axis=1)
    X = X.drop("DayOfWeek", axis=1)

    return X

