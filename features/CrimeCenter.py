import pandas as pd
import numpy as np
import math

def feature_CrimeSimilarity(featureData):

    data = pd.read_csv('data/train.csv')
    dataM = featureData.as_matrix()
    district = data['PdDistrict']
    districtU = district.unique()
    category = data['Category']
    popCategory = category.value_counts(normalize=False, sort=True, ascending=False, bins=None,)[:15]
    policeDepartment = featureData['PdDistrict'].as_matrix()

    #mean  x,y coordinates grouped by district and Category
    x_mean = data['X'].groupby([data['PdDistrict'],data['Category']]).mean()
    y_mean = data['Y'].groupby([data['PdDistrict'],data['Category']]).mean()

    x = pd.to_numeric(featureData['X'].as_matrix())
    y = pd.to_numeric(featureData['Y'].as_matrix())


    X_meanTable = np.zeros((districtU.shape[0], (popCategory.shape[0])))
    Y_meanTable = np.zeros((districtU.shape[0], (popCategory.shape[0])))

    for i in range(0, districtU.shape[0]):

        for j in range(0, popCategory.shape[0]):

            X_meanTable[i][j] = x_mean[districtU[i]][popCategory.index[j]]
            Y_meanTable[i][j] = y_mean[districtU[i]][popCategory.index[j]]


    df = np.zeros((dataM.shape[0],popCategory.size))
    names = []

    for t in range(0, popCategory.size):

        names.append('similarity_' + str(popCategory.index[t]))

        for i in range(0, dataM.shape[0]):

            for j in range(0, districtU.size):

                if (policeDepartment[i] == districtU[j]):

                    df[i][t] = (X_meanTable[j][t]*x[i] + Y_meanTable[j][t]*y[i])/ (math.sqrt(X_meanTable[j][t]**2 + \
                                                                                             Y_meanTable[j][t]**2) * \
                                                                                   math.sqrt(x[i]**2 + y[i]**2))
    return np.asarray(df), names

"""def feature_CrimeDistance(data):
    dataM = data.as_matrix()
    district = data['PdDistrict']
    districtU = district.unique()
    category = data['Category']
    popCategory = category.value_counts(normalize=False, sort=True, ascending=False, bins=None,)[:15]
    policeDepartment = data['PdDistrict'].as_matrix()

    #mean  x,y coordinates grouped by district and Category
    x_mean = data['X'].groupby([data['PdDistrict'],data['Category']]).mean()
    y_mean = data['Y'].groupby([data['PdDistrict'],data['Category']]).mean()

    x = pd.to_numeric(data['X'].as_matrix())
    y = pd.to_numeric(data['Y'].as_matrix())


    X_meanTable = np.zeros((districtU.shape[0], (popCategory.shape[0])))
    Y_meanTable = np.zeros((districtU.shape[0], (popCategory.shape[0])))

    for i in range(0, districtU.shape[0]):

        for j in range(0, popCategory.shape[0]):

            X_meanTable[i][j] = x_mean[districtU[i]][popCategory.index[j]]
            Y_meanTable[i][j] = y_mean[districtU[i]][popCategory.index[j]]


    df = np.zeros((dataM.shape[0],popCategory.size))
    names = []

    for t in range(0, popCategory.size):

        names.append('distance_' + str(popCategory.index[t]))

        for i in range(0, dataM.shape[0]):

            for j in range(0, districtU.size):

                if (policeDepartment[i] == districtU[j]):




    return np.asarray(df), names"""

def feature_CrimeDistance(featureData):

    data = pd.read_csv('data/train.csv')
    dataM = featureData.as_matrix()
    district = data['PdDistrict']
    districtU = district.unique()
    category = data['Category']
    popCategory = category.value_counts(normalize=False, sort=True, ascending=False, bins=None,)[:15]
    policeDepartment = featureData['PdDistrict'].as_matrix()

    #mean  x,y coordinates grouped by district and Category
    x_mean = data['X'].groupby([data['PdDistrict'],data['Category']]).mean()
    y_mean = data['Y'].groupby([data['PdDistrict'],data['Category']]).mean()

    x = pd.to_numeric(featureData['X'].as_matrix())
    y = pd.to_numeric(featureData['Y'].as_matrix())


    X_meanTable = np.zeros((districtU.shape[0], (popCategory.shape[0])))
    Y_meanTable = np.zeros((districtU.shape[0], (popCategory.shape[0])))

    for i in range(0, districtU.shape[0]):

        for j in range(0, popCategory.shape[0]):

            X_meanTable[i][j] = x_mean[districtU[i]][popCategory.index[j]]
            Y_meanTable[i][j] = y_mean[districtU[i]][popCategory.index[j]]


    df = np.zeros((dataM.shape[0],popCategory.size))
    names = []

    for t in range(0, popCategory.size):

        names.append('distance_' + str(popCategory.index[t]))

        for i in range(0, dataM.shape[0]):

            for j in range(0, districtU.size):

                if (policeDepartment[i] == districtU[j]):

                    df[i][t] = 1/(math.sqrt((x[i] - X_meanTable[j][t]) ** 2 + (y[i] - Y_meanTable[j][t]) ** 2))

    dFrame = pd.DataFrame(np.asarray(df))
    dFrame = (dFrame - dFrame.mean())/dFrame.std()

    return np.asarray(dFrame), names

